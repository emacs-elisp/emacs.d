* Feeds                                                              :elfeed:

** FPGA                                                               :fpga:

*** http://www.fpgadeveloper.com/feed                      :fpga_developer:
*** http://billauer.co.il/blog/feed/atom/                        :billauer:
*** https://blog.hackster.io/feed/tagged/microzed         :microzed:medium:

** emacs                                                             :emacs:

*** [[http://www.reddit.com/r/emacs/.rss]]                             :reddit:
*** [[http://emacs.stackexchange.com/feeds]]                                                      :stackexchange:

*** Blogs                                                           :blogs:

**** [[https://emacsair.me/feed.xml]]                               :emacsair:
**** [[http://batsov.com/atom.xml]]                                   :batsov:
**** [[http://feeds.feedburner.com/XahsEmacsBlog]]                       :xah:
**** [[http://feeds.feedburner.com/GotEmacs?format=xml]]            :gotemacs:
**** [[http://emacsist.com/rss]]                                    :emacsist:
**** [[http://emacsredux.com/atom.xml]]                           :emacsredux:
**** [[http://www.masteringemacs.org/feed]]                        :mastering:
**** [[http://feeds.feedburner.com/emacsblog]]                :allthingsemacs:
**** [[http://www.lunaryorn.com/feed.atom][Lunaryon]]                                                  :lunarsite:
**** [[http://oremacs.com/atom.xml]]                                 :oremacs:
**** [[http://endlessparentheses.com/atom.xml]]           :endlessparentheses:
**** [[http://feeds.feedburner.com/WisdomAndWonder]]         :wisdomandwonder:
**** [[http://www.howardism.org/index.xml]]                        :howardism:
**** [[http://pragmaticemacs.com/feed/]]                           :pragmatic:
**** [[http://emacsrocks.com/atom.xml]]                          :emacsrocks:
**** [[http://emacspeak.blogspot.com/feeds/posts/default]]         :emacspeak:
**** [[http://whattheemacsd.com/atom.xml]]                     :whattheemacsd:
**** [[http://emacs-fu.blogspot.com/feeds/posts/default?alt=rss]]    :emacsfu:
**** [[http://jr0cket.co.uk/atom.xml]]                               :jr0cket:
**** [[http://blog.binchen.org/rss.xml]]                             :chenbin:
**** [[http://emacsmovies.org/atom.xml]]                         :emacsmovies:
**** [[http://sachachua.com/blog/category/emacs/feed][Sacha Chua]]                                                   :sachac:
**** [[http://www.modernemacs.com/post/outline-ivy/][ModernEmacs]]                                                  :modern:
**** [[https://ambrevar.xyz/rss.xml][Ambrevar]]                                                   :ambrevar:
**** [[https://www.with-emacs.com/rss.xml][With-emacs]]                                               :with_emacs:
**** [[https://cestlaz.github.io/tags/emacs/rss][cestlaz]]                                                     :cestlaz:
**** [[https://jonathanabennett.github.io/rss.xml][Jonathan Bennett]]                                            :bennett:

**** Null program                                            :nullprogram:

***** [[http://nullprogram.com/tags/lisp/feed]]                        :lisp:
***** [[http://nullprogram.com/tags/elisp/feed]]                      :elisp:
***** [[http://nullprogram.com/tags/emacs/feed]]

**** Kitchin                                                     :kitchin:

***** [[http://kitchingroup.cheme.cmu.edu/blog/category/emacs/feed]]
***** [[http://kitchingroup.cheme.cmu.edu/blog/category/emacs-lisp/feed]] :elisp:
***** [[http://kitchingroup.cheme.cmu.edu/blog/category/emacs_lisp/feed]] :elisp:
***** [[http://kitchingroup.cheme.cmu.edu/blog/category/elisp/feed]] :elisp:
***** [[http://kitchingroup.cheme.cmu.edu/blog/category/orgmode/feed]] :orgmode:
***** [[http://kitchingroup.cheme.cmu.edu/blog/category/org/feed]]  :orgmode:

**** Irreal                                                       :irreal:

***** [[http://irreal.org/blog/?feed=rss2]]
***** [[http://irreal.org/blog/?feed=comments-rss2]]               :comments:

*** Planets                                                        :planet:

**** [[http://planet.emacsen.org/atom.xml]]
**** http://planet.emacs-es.org/rss20.xml

** Dummy                                                    :unread:starred:
