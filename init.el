(setq MyHomeDir (getenv "HOME"))

(setq org-config-path "")

(defvar csb/perso-config (file-exists-p "/home/csantos")
  "Flag to selectively tangle code")

(defvar running-os-is-linux (eq system-type 'gnu/linux)
  "OS used as bootloader for emacs")

(setq gc-cons-threshold (* 50 1000 1000))

(add-hook 'after-init-hook
          (lambda ()
            (setq gc-cons-threshold (* 2 1000 1000))))

(when (boundp 'w32-pipe-read-delay)
  (setq w32-pipe-read-delay 0))

(when (file-exists-p (format "%s/proxy.el" MyHomeDir))
  (load-file (format "%s/proxy.el" MyHomeDir)))

(require 'package)

(setq package-user-dir (format "%s/.emacs.d/elpa" MyHomeDir))

(setq load-prefer-newer t
      package-enable-at-startup t)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(setq package-menu-hide-low-priority t
      package-archive-priorities
      '( ("gnu" . 15)
         ("org" . 30)
         ;;("elpy" . 25)
         ("melpa-stable" . 20)
         ("melpa" . 10)))

(package-initialize)
;; (package-refresh-contents)

;; (setq csb/idle-refresh-timer
;;       (run-with-idle-timer (* 60 60) t 'package-refresh-contents))

;; (when (not (package-installed-p 'use-package))
;;   (package-install 'use-package))

(setq use-package-always-ensure t)

(when (not (package-installed-p 'use-package))
  (error "No use-package installed locally, check your network and restart emacs. Aborting."))

(defun with-initial-minibuffer (str)
  (interactive)
  (funcall `(lambda ()
              (interactive)
              (minibuffer-with-setup-hook
                  (lambda () (insert (format "%s " ,str)))
                (call-interactively 'helm-M-x)))))

(defvar csb/defer 10 "Use to defer loading of packages")

(define-prefix-command 'launcher-map)
(define-key ctl-x-map "l" 'launcher-map)

(defun csb/internet-up-p (&optional host)
  (or (= 0
         (if url-proxy-services
             (call-process "ping" nil nil nil "-n" "1" "-w" "1"
                           (or host "proxy.indra.es"))
           (call-process "ping" nil nil nil "-c" "1" "-W" "1"
                         (or host "www.google.com"))))
      (progn
        (message "no network !!")
        nil)))

(defun csb/org-require(feature &optional disable-download compileit)
  "Load a ‘FEATURE', downloading it from online when not ‘DISABLE-DOWNLOAD' and network
is available, and ‘COMPILEIT' if necessary"
  (let ((orgfilename (format "%scsb-%s.org" user-emacs-directory (symbol-name feature)))
        (pagefilename (format "csb-%s.org" (symbol-name feature)))
        (elfilename (format "csb-%s.el" (symbol-name feature)))
        (host (if url-proxy-services
                  "proxy.indra.es"
                "www.google.com")))
    (cond (;; When network is up or doesn’t disables it, download up to date orgfile, and
           ;; org-load it
           (and (not disable-download) (csb/internet-up-p host))
           (url-copy-file
            (format "http://gitlab.com/csantosb/wikidata/raw/master/emacs.cat/org-config.cat/%s" pagefilename)
            orgfilename t)
           (org-babel-load-file orgfilename compileit)
           (message (format "Loading (remote) feature ... %s" (symbol-name feature))))
          ((file-exists-p (format "%s%s" user-emacs-directory elfilename))
           (load-file (format "%s%s" user-emacs-directory elfilename))
           (message (format "Loading (local) feature ... %s" (symbol-name feature))))
          (t
           (message (format "%s feature is not loaded" (symbol-name feature)))))))

(use-package hydra
  :defer csb/defer
  :config
  (require 'posframe)
  (setq hydra-hint-display-type 'posframe))

(setq ls-lisp-use-insert-directory-program t
      insert-directory-program "ls")

(use-package org
  :ensure org-plus-contrib
  :config
  (setq org-return-follows-link t
        inhibit-compacting-font-caches t)
  :hook (org-mode . (lambda ()
                      (aggressive-fill-paragraph-mode t)))
  :bind
  (("C-x L" . org-store-link)		;; global
   ("C-x C-l" . org-insert-link-global) ;; global
   :map org-mode-map
   ("C-x C-l" . org-insert-link)))

(use-package org-link-minor-mode
  :defer csb/defer)

(use-package poporg
  :defer csb/defer
  :bind
  ;; call it
  (:map global-map
        (("C-c SPC" . poporg-dwim))
        ;; from within the org mode, poporg-mode-map is a minor mode
        :map poporg-mode-map
        ("C-c C-c" . poporg-update)            ;; update original
        ("C-c SPC" . poporg-edit-exit)         ;; exit, keeping changes
        ("C-x C-s" . poporg-update-and-save))  ;; update original and save buffer
  :hook (poporg-mode . (lambda ()
                         (outline-show-all)
                         (aggressive-fill-paragraph-mode t)
                         (goto-char (point-min)))))

(use-package outshine
  :defer csb/defer
  :pin melpa
  :config
  (setq outshine-use-speed-commands t
        outshine-startup-folded-p nil)

  (define-key outline-minor-mode-map (kbd "<backtab>") 'outshine-cycle-buffer)
  (define-key outline-minor-mode-map (kbd "C-RET") 'outshine-insert-heading)

  (outshine-define-key-with-fallback
      outshine-mode-map (kbd "C-c C-n")
    (outshine-speed-move-safe (quote outline-next-visible-heading))
    t)

  (outshine-define-key-with-fallback
      outshine-mode-map (kbd "C-c C-p")
    (outshine-speed-move-safe (quote outline-previous-visible-heading))
    t)

  (outshine-define-key-with-fallback
      outshine-mode-map (kbd "C-c C-f")
    (outshine-speed-move-safe (quote outline-forward-same-level))
    (outline-on-heading-p))

  (outshine-define-key-with-fallback
      outshine-mode-map (kbd "C-c C-b")
    (outshine-speed-move-safe (quote outline-backward-same-level))
    (outline-on-heading-p))

  (outshine-define-key-with-fallback
      outshine-mode-map (kbd "C-c C-u")
    (outshine-speed-move-safe (quote outline-up-heading))
    (outline-on-heading-p))
  )

(use-package company
  :defer csb/defer
  :init (global-company-mode t)
  :config
  ;; Company Quickhelp
  ;; When idling on a completion candidate the documentation for the
  ;; candidate will pop up after `company-quickhelp-delay' seconds.
  (use-package company-quickhelp
    :defer csb/defer
    ;; :if window-system
    :init (company-quickhelp-mode t))
  ;; With use-package:
  ;; (use-package company-box
  ;;   :hook (company-mode . company-box-mode))
  ;; Variables
  (setq company-dabbrev-ignore-case nil
        company-dabbrev-code-ignore-case nil
        company-dabbrev-downcase nil
        company-idle-delay 0.01
        company-echo-delay 0.01
        company-minimum-prefix-length 2)
  :diminish company-mode)

(use-package flycheck-pos-tip
  :defer csb/defer)
(use-package helm-flycheck
  :defer csb/defer)
(use-package flycheck-color-mode-line
  :defer csb/defer)
(use-package flycheck
  :init
  (global-flycheck-mode t)
  (flycheck-pos-tip-mode t)
  :bind
  (:map flycheck-command-map
        ("h" . helm-flycheck))
  :config
  (setq flycheck-completion-system 'helm-comp-read )
  (add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

(use-package aggressive-fill-paragraph
  :defer csb/defer)

(use-package disable-mouse
  :defer csb/defer
  :init (global-disable-mouse-mode 1)) ; stop using the mouse !

(use-package auto-compile
  :defer csb/defer
  :init
  (auto-compile-on-load-mode 1)
  (auto-compile-on-save-mode 1)
  :config
  (setq auto-compile-display-buffer nil
        auto-compile-mode-line-counter t
        load-prefer-newer t
        auto-compile-display-buffer nil
        auto-compile-mode-line-counter t))

(use-package smart-mode-line
  :init
  (setq sml/no-confirm-load-theme t)
  (sml/setup)
  :config
  ;; I do wanna see the backend in use
  (setq rm-whitelist " company.*")
  ;; (setq rm-blacklist '(" hl-p" " Fill" " OrgStruct" " SliNav" " AC" " Google" " Undo-Tree" " Helm" " Spell"
  ;;                 " ElDoc" " company" " yas" " Abbrev" " PgLn" " Paredit" " *" " Wrap" " WSC" " Fly"
  ;;                 " MRev" " +Chg" " Guide" " wg" " ||" " =>" "Projectile" " Ind" " GG" " NoMouse!"
  ;;                 " super-save" " h" " HelmGtags" " VHl" " #vtool"))
  (setq sml/mode-width 'right
        sml/line-number-format "%5l"
        sml/shorten-modes t
        sml/shorten-directory t)
  (sml/apply-theme "respectful"))

(add-hook 'prog-mode-hook
          (lambda ()
            (outshine-mode t)
            (org-link-minor-mode t)))

(use-package helm
  :init
  (require 'helm-config)
  (helm-mode 1)
  (helm-autoresize-mode 1)
  :defer csb/defer
  :bind (("M-x" . helm-M-x)
         ("C-x C-b" . helm-buffers-list)
         ("C-x b" . helm-mini)
         ("C-x C-f" . helm-find-files)
         ("M-y" . helm-show-kill-ring)))

(use-package helm-themes
  :defer csb/defer
  :after helm
  :bind
  (:map helm-command-map
        ("T" . 'helm-themes)))

(use-package projectile
  :init (projectile-mode 1)
  ;; Keys
  :bind
  (:map projectile-mode-map
        ("C-c p" . projectile-command-map)
        ("C-c p s a" . projectile-ag)
        ("C-c p s g" . projectile-grep)
        ("C-c p ?" . hydra-projectile/body)
        ("C-c p SPC" . hydra-projectile/body)
        ("C-c p s s" . nil)
        ("C-c p s r" . projectile-ripgrep))
  :config

  (let ((x 0))
    (cl-loop until (>= x 100) do
             (add-to-list 'projectile-globally-ignored-files
                          (format ".~%s~" x))
             (setq x (+  1 x))))

  ;; Variables
  (setq projectile-completion-system 'helm
        projectile-indexing-method 'alien
        projectile-enable-caching t
        projectile-switch-project-action 'projectile-dired)

  (add-to-list 'projectile-globally-ignored-directories ".backups")
  (add-to-list 'projectile-globally-ignored-directories ".stversions"))

(use-package helm-projectile
  :init (helm-projectile-on)
  :config
  (setq
   helm-projectile-fuzzy-match t
   helm-projectile-sources-list '(helm-source-projectile-recentf-list
                                  helm-source-projectile-buffers-list
                                  helm-source-projectile-files-list
                                  helm-source-projectile-projects)))

(global-set-key (kbd "C-s")
                (lambda (&optional arg)
                  (interactive "P")
                  (if (equal arg '(4))
                      (isearch-forward-symbol-at-point)
                    (isearch-forward))))

(global-set-key (kbd "C-r")
                (lambda (&optional arg)
                  (interactive "P")
                  (if (equal arg '(4))
                      (progn
                        (isearch-forward-symbol-at-point)
                        (let ((superword-mode t))
                          (backward-word 1)))
                    (isearch-backward))))

(use-package helm-ag
  :defer csb/defer)

(use-package ripgrep
  :defer csb/defer)

(use-package helm-rg
  :defer csb/defer)

(use-package helm-swoop
  :defer csb/defer
  :bind
  (:map helm-command-map
        ("u" . csb/helm-swoop)
        ("p" . helm-swoop)
        ("P" . helm-multi-swoop))
  (:map helm-swoop-map
        ;; From helm-swoop to helm-multi-swoop-all
        ("M-w" . helm-multi-swoop-all-from-helm-swoop))
  :commands (helm-swoop helm-multi-swoop helm-multi-swoop-all helm-multi-swoop-org helm-multi-swoop-current-mode)
  :config
  (setq
   helm-swoop-pre-input-function (lambda () nil)
   ;; Save buffer when helm-multi-swoop-edit complete
   helm-multi-swoop-edit-save t
   ;; If this value is t, split window inside the current window
   helm-swoop-split-with-multiple-windows t
   ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
   helm-swoop-split-direction 'split-window-vertically
   ;; If nil, you can slightly boost invoke speed in exchange for text color
   helm-swoop-speed-or-color t
   ;; Go to the opposite side of line from the end or beginning of line
   helm-swoop-move-to-line-cycle t
   ;; Face name is `helm-swoop-line-number-face`
   helm-swoop-use-line-number-face t)

  (defun csb/helm-swoop (&optional arg)
    "Defaults to ‘helm-navi-headings’; with prefix ‘ARG’ use ‘helm-navi’ instead.
     Opens new window below or aside following current window width.
    Calls a custom action, defined under current major mode."
    (interactive "P")
    (let* ((helm-swoop-split-direction
            (if (> (window-width) 105)
                'split-window-horizontally
              'split-window-vertically ))
           (mycommentstart comment-start)
           (mycommentpadding comment-padding)
           (helm-swoop-pre-input-function
            (lambda ()
              (if (string= major-mode "emacs-lisp-mode")
                  ";;; "
                (format "%s * " comment-start comment-padding)))))
      (cond ((equal arg '(16))
             ;; general dispatcher
             (csb/hydra-helm-swoop/body))
            ((equal arg '(4))
             ;; Applies all buffers of the same mode as the current buffer to helm-multi-swoop
             (helm-multi-swoop-current-mode))
            (t
             (helm-swoop))))))

(use-package with-editor
  :init
  (shell-command-with-editor-mode t)
  :config
  (define-key (current-global-map)
    [remap async-shell-command] 'with-editor-async-shell-command)
  (define-key (current-global-map)
    [remap shell-command] 'with-editor-shell-command)
  :hook
  ((shell-mode . with-editor-export-editor)
   (term-exec . with-editor-export-editor)
   (eshell-mode . with-editor-export-editor)))

(use-package magit-svn
  :defer csb/defer
  :after magit
  :hook
  (magit-mode . magit-svn-mode))

(use-package magit
  :defer csb/defer
  :config
  ;; (remove-hook 'server-switch-hook 'magit-commit-diff)
  (setq magit-diff-paint-whitespace nil
        magit-diff-refine-hunk nil
        magit-diff-highlight-trailing t
        magit-diff-highlight-indentation t
        magit-refresh-verbose nil
        magit-refresh-status-buffer nil
        magit-display-buffer-function
        #'magit-display-buffer-fullframe-status-v1))

(use-package ssh-agency
  :after magit
  :config
  (setenv "SSH_ASKPASS" "git-gui--askpass"))

(use-package ediff
  :defer csb/defer
  :hook
  ((ediff-mode . ediff-setup-keymap)
   (ediff-prepare-buffer . outline-show-all))
  ;; :bind
  ;; ;; Assign j to move down, and k to move up.
  ;; (:map ediff-mode-map
  ;;       ("j" . ediff-next-difference)
  ;;       ("k" . ediff-previous-difference))
  :config
  (winner-mode 1)
  ;; Restore the previous windows layout after quitting ediff
  (add-hook 'ediff-after-quit-hook-internal 'winner-undo)
  ;; Variables
  (setq ediff-window-setup-function #'ediff-setup-windows-plain
        ediff-split-window-function #'split-window-horizontally
        ;; ignore whitespace
        ediff-diff-options "-w")
  ;; If nil, only the selected differences are highlighted.
  (setq-default ediff-highlight-all-diffs nil)
  (setq-default ediff-auto-refine 'on))

(defhydra csb/hydra-ediff (:body-pre nil
                                     :color blue
                                     :hint nil)
  "
             _b_uffers      _f_iles     _d_irectories    d_w_im
           _3_ buffers3   _4_ files3  _5_ directories3  _r_evision

    Regions  _l_inewise     _w_ordwise

     _
      "
  ("b" ediff-buffers)
  ("f" ediff-files)
  ("r" ediff-revision)
  ("d" ediff-directories)
  ("w" ediff-dwim)
  ("3" ediff-buffers3)
  ("4" ediff-files3)
  ("5" ediff-directories3)
  ("l" ediff-regions-linewise)
  ("w" (progn
         (global-set-key (kbd "C-c C-c") 'exit-recursive-edit)
         (global-set-key (kbd "C-c C-k") 'abort-recursive-edit)
         (call-interactively 'ediff-regions-wordwise))))

(defhydra csb/hydra-vc (:color red
                               :hint nil
                               :exit t )
  "
_q_uit        _g_ ^ ^ git gutter  - info           _c_  vc      - vc
^ ^           _e_ ^ ^ ediff       - ediff          _s_  smerge  - smerge
^ ^           _v_ ^ ^ vdiff       - vdiff
^ ^           _h_/_H_ helm hunks  - csb/helm-hunks(staged)
  .
          "
  ("g" hydra-git-gutter/body)
  ("e" csb/hydra-ediff/body)
  ("v" csb/hydra-vdiff/body)
  ("s" csb/hydra-smerge/body)
  ("h" csb/helm-hunks)
  ("c" csb/hydra-vc/body)
  ("H" csb/helm-hunks-staged)
  ("q" nil))

(define-key ctl-x-map "v" #'csb/hydra-vc/body)
(define-key launcher-map "v" #'csb/hydra-vc/body)

(global-set-key (kbd "C-=") (lambda () (interactive) (text-scale-adjust 0)))
(global-set-key (kbd "C-+") (lambda () (interactive) (text-scale-increase 0.5)))
(global-set-key (kbd "C--") (lambda () (interactive) (text-scale-decrease 0.5)))
(global-set-key (kbd "C-x k") #'kill-this-buffer)
(global-set-key (kbd "C-x g") #'magit-status)

(define-prefix-command 'launcher-map)
(define-key ctl-x-map "l" 'launcher-map)

(setq-default cursor-type '(hbar . 2)
              confirm-kill-emacs nil
              inhibit-splash-screen t
              inhibit-startup-message t
              ad-redefinition-action 'accept                   ; Silence warnings for redefinition
              confirm-kill-emacs 'yes-or-no-p                  ; Confirm before exiting Emacs
              cursor-in-non-selected-windows t                 ; Hide the cursor in inactive windows
              delete-by-moving-to-trash t                      ; Delete files to trash
              display-time-default-load-average nil            ; Don't display load average
              display-time-format "%H:%M"                      ; Format the time string
              fill-column 80                                   ; Set width for automatic line breaks
              help-window-select t                             ; Focus new help windows when opened
              indent-tabs-mode nil                             ; Stop using tabs to indent
              inhibit-startup-screen t                         ; Disable start-up screen
              initial-scratch-message ""                       ; Empty the initial *scratch* buffer
              left-margin-width 1 right-margin-width 1         ; Add left and right margins
              mode-require-final-newline 'visit                ; Add a newline at EOF on visit
              mouse-yank-at-point t                            ; Yank at point rather than pointer
              ns-use-srgb-colorspace nil                       ; Don't use sRGB colors
              recenter-positions '(5 top bottom)               ; Set re-centering positions
              redisplay-dont-pause t                           ; don't pause display on input
              debug-on-error t
              jit-lock-defer-time 0
              frame-resize-pixelwise t
              fast-but-imprecise-scrolling t
              scroll-conservatively 10000                      ; Always scroll by one line
              scroll-margin 1                                  ; scroll N lines to screen edge
              scroll-step 1                                    ; keyboard scroll one line at a time
              scroll-preserve-screen-position 1
              select-enable-clipboard t                        ; Merge system's and Emacs' clipboard
              sentence-end-double-space nil                    ; End a sentence after a dot and a space
              show-trailing-whitespace nil                     ; Display trailing whitespaces
              split-height-threshold nil                       ; Disable vertical window splitting
              split-width-threshold nil                        ; Disable horizontal window splitting
              tab-width 4                                      ; Set width for tabs
              uniquify-buffer-name-style 'forward              ; Uniquify buffer names
              window-combination-resize t                      ; Resize windows proportionally
              x-stretch-cursor t)                              ; Stretch cursor to the glyph width

(delete-selection-mode)                           ; Replace region when inserting text
(setq line-number-mode t)                         ; Enable line numbers in the mode-line
(setq column-number-mode t)                       ; Enable column numbers in the mode-line
(size-indication-mode 1)                          ; Enable size status in the mode-line
(display-time-mode)                               ; Enable time in the mode-line
(when (boundp 'fringe-mode)
  (fringe-mode 80))
(fset 'yes-or-no-p 'y-or-n-p)                     ; Replace yes/no prompts with y/n
(global-hl-line-mode)                             ; Hightlight current line
(show-paren-mode t)
(setq show-paren-style 'expression)
(global-subword-mode)                             ; Iterate through CamelCase words
(menu-bar-mode 0)                                 ; Disable the menu bar
(tool-bar-mode 0)                                 ; Disable the tool bar
(tooltip-mode 0)                                  ; Disable the tooltips
(blink-cursor-mode t)                             ; Disable the cursor blinking
(when (boundp 'scroll-bar-mode)
  (scroll-bar-mode 0))                            ; Disable the scroll bar
(mouse-avoidance-mode 'banish)                    ; Avoid collision of mouse with point
(put 'downcase-region 'disabled nil)              ; Enable downcase-region
(put 'upcase-region 'disabled nil)                ; Enable upcase-region
(put 'downcase-region 'disabled nil)              ; Enable downcase-region
(put 'upcase-region 'disabled nil)                ; Enable upcase-region
(add-hook 'focus-out-hook #'garbage-collect)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(global-display-line-numbers-mode t)

(set-language-environment 'utf-8)
(setq locale-coding-system 'utf-8-unix)
(prefer-coding-system 'utf-8-unix)
(set-default-coding-systems 'utf-8-unix)
(set-terminal-coding-system 'utf-8-unix)
(set-keyboard-coding-system 'utf-8-unix)
(set-selection-coding-system 'utf-8-unix)

(when (eq (window-system) 'w32)
  (setq shell-file-name "c:/msys64/usr/bin/sh.exe"))

(when (file-exists-p (format "%s/.emacs.d/init-user.el" MyHomeDir))
  (load-file (format "%s/.emacs.d/init-user.el" MyHomeDir)))

(setq custom-file (format "%s/.emacs.d/emacs-custom" MyHomeDir))
(when (file-exists-p (format "%s/.emacs.d/emacs-custom" MyHomeDir))
  (load-file (format "%s/.emacs.d/emacs-custom" MyHomeDir)))
