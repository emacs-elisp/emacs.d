;; Define here custom functionality

;;; General

;;;; Variables

;;;; Keys

;;; Timers

;; You may cancel the timer defined in main init.el with

;; (cancel-timer csb/idle-refresh-timer)

;; To perform a given action at some fix time, use this instead

;; (run-at-time "20:00pm" nil 'package-refresh-contents)

;; See documentation of run-at-time (with C-h f) for details

;;; Locales

;; set the locales, if necessary
;; (setenv "LANG" "fr_FR.UTF-8")
;; (set-locale-environment)
;; (set-input-method "french-azerty")

;;; Features

;; Check features section in init.org file

;; When true, disable downloading of features, and use latest local version instead
(defvar disable-download nil "Disables downloading of features.")

;;;; Needed features

;; Declare a list of needed features to load
;; Convenience and miscellaneous features are highly recommended.
(defvar neededfeatures '(miscellaneous convenience) "List of needed features to load.")

;; Load needed features
(dolist (feature neededfeatures)
  (csb/org-require feature disable-download t))

;;;; Optional features

;; Declare a list of optional features to load
(defvar myfeatures nil "List of features to load.")

;; User may select some or all of
(setq myfeatures '(dired parenthesis vc terminal backup
			 search helm outshine elisp compile
			 project-management completions))

;; Load optional features
(dolist (feature myfeatures)
  (csb/org-require feature disable-download t))

;; Below sections provide further customization and relevant information to each feature

;;;;; Convenience

;; Load [[https://gitlab.com/csantosb/wikidata/blob/master/emacs.cat/org-config.cat/csb-convenience.page][convenience]] feature

;; Provides jumping to next / previous instance of symbol at point with =M-n=, =M-p=. Prior,
;; think activating [[https://emacsredux.com/blog/2013/04/21/camelcase-aware-editing/][camel case]] (with subword mode) or [[https://emacsredux.com/blog/2014/08/27/a-peek-at-emacs-24-dot-4-superword-mode/][superword]] modes to tune
;; =smartscan= to your coding style.

;;;;; Miscellaneous

;; Load [[https://gitlab.com/csantosb/wikidata/blob/master/emacs.cat/org-config.cat/csb-miscellaneous.page][miscellaneous]] feature

;;;;; Lsp

;;;;; Dired

;;;;; Parenthesis

;;;;; Version control

;;;;; Terminal

;;;;; Hydra

;; (with-eval-after-load 'hydra
;;   (setq hydra-hint-display-type 'lv))

;;;;; Search

(when (member 'search myfeatures)
  (define-key launcher-map "s" #'csb/hydra-search/body))

;;;;; Outshine

;;;;; Project management

;;;;; Elfeed

(use-package elfeed

  :defer t

  :init

  (defun csb/elfeed ()
    "Launch elfeed in its persp and create related timers."
    (interactive)
    ;; (persp-switch "elfeed")
    ;; if no elfeed buffer aroung, launch it
    (unless (get-buffer "*elfeed-search*")
      (csb/org-require 'elfeed disable-download t)
      (csb/org-require 'modal disable-download t)
      (elfeed-db-load)
      (elfeed)
      (when (csb/internet-up-p)
        ;; Update for feeds each time emacs is idle for 10 min
        (setq csb/elfeed-timer-update
              (run-with-idle-timer 600 t 'elfeed-update))
        ;; Force refresh view of the feed listing
        (setq csb/elfeed-timer-update--force
              (run-with-idle-timer 700 t 'elfeed-search-update--force))
        ;; Save and minimize the database storage size on the filesystem
        (setq csb/elfeed-timer-db-compact
              (run-with-idle-timer 1200 t 'elfeed-db-compact))))
    (switch-to-buffer (get-buffer "*elfeed-search*"))
    ;; Force refresh view of the feed listing.
    (elfeed-search-update--force))

  :bind
  (:map launcher-map
        (("e" . csb/elfeed)))

  :config

  (defun csb/identify-monitor ()
    56))

;;;;; Completions

;;; Programming modes

;;;; Vhdl

;; Load [[https://gitlab.com/csantosb/wikidata/blob/master/emacs.cat/org-config.cat/csb-vhdl.page][vhdl]] feature. For its use, refer to the =Vhdl= section of the =init.org= file.

(use-package vhdl-mode
  :defer t
  :config
  (csb/org-require 'lsp disable-download t)
  (csb/org-require 'vhdl disable-download t))

;;;; Tcl

(use-package tcl
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("\\.vsf\\'". tcl-mode))
  (add-to-list 'auto-mode-alist '("\\.xdc\\'". tcl-mode))
  (add-to-list 'auto-mode-alist '("tclIndex". tcl-mode))
  :config
  (csb/org-require 'tcl disable-download t)
  :hook
  (tcl-mode . (lambda ()
		(aggressive-indent-mode -1))))

;;;; Elisp

(use-package elisp-mode
  :defer t
  :ensure nil
  :config (csb/org-require 'elisp disable-download t))

;;; Theme

;; load a default theme
(use-package gruvbox-theme
  :defer nil
  :if (package-installed-p 'gruvbox-theme)
  :config (load-theme 'gruvbox-dark-medium t))

;;; Appearance

;; Start maximised
(add-hook 'window-setup-hook 'toggle-frame-fullscreen t)

;; Start new frames full screen
(add-to-list 'default-frame-alist '(fullscreen . fullboth))

;; Remove all that’s not necessary
(global-display-line-numbers-mode -1)
;; (blink-cursor-mode 0)                   ; Disable the cursor blinking

(message "Loaded user init file. Done.")

(when (file-exists-p (format "%s/.emacs.d/init-user-custom.el" MyHomeDir))
  (load-file (format "%s/.emacs.d/init-user-custom.el" MyHomeDir)))
