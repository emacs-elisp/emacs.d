;;; Fonts

;; (with-eval-after-load 'outshine-mode
;;   (custom-theme-set-faces 'user
;;                           `(outline-1 ((t (:inherit org-level-1 :height 1.1 :family "Symbola"))))
;;                           `(outline-2 ((t (:inherit org-level-2 :height 1.05 :family "Symbola"))))
;;                           `(outline-3 ((t (:inherit org-level-3 :height 1.05 :family "Symbola"))))
;;                           `(outline-4 ((t (:inherit org-level-4 :height 1.05 :family "Symbola"))))
;;                           `(outline-5 ((t (:inherit org-level-5 :height 1.05 :family "Symbola"))))))

;; (with-eval-after-load 'org-mode
;;   (custom-theme-set-faces 'user
;;			  `(org-level-1 ((t (:weight medium :height 1.3 :family "Symbola"))))
;;			  `(org-level-2 ((t (:weight medium :height 1.2 :family "Symbola"))))
;;			  `(org-level-3 ((t (:weight medium :height 1.1 :family "Symbola"))))
;;			  `(org-level-4 ((t (:weight medium :height 1.1 :family "Symbola"))))
;;			  `(org-level-5 ((t (:weight medium :height 1.1 :family "Symbola"))))
;;			  `(org-level-6 ((t (:weight medium :height 1.1 :family "Symbola"))))
;;			  `(org-default ((t (:family "Symbola"))))))

;; (unless (string= (system-name) "ArchLinux-VM")
;;   (defvar mp/font-family            "Inconsolata" "The font to use.")
;;   (defvar mp/font-size-default      120        "The font size to use for default text.")
;;   (defvar mp/font-size-header-line  120        "The font size to use for the header-line.")
;;   (defvar mp/font-size-mode-line    120        "The font size to use for the mode-line.")
;;   (defvar mp/font-size-title        120        "The font size to use for titles.")

;;   (set-face-attribute 'font-lock-comment-face nil :italic t)
;;   (set-face-attribute 'font-lock-doc-face nil :italic t)
;;   (set-face-attribute 'default nil :height mp/font-size-default :font mp/font-family)
;;   (set-face-attribute 'header-line nil :height mp/font-size-header-line)
;;   (set-face-attribute 'mode-line nil :height mp/font-size-mode-line)
;;   (set-face-attribute 'mode-line-inactive nil :height mp/font-size-mode-line))

;;;; Helm

;; (when (fboundp 'helm)
;;   (setq helm-display-function #'helm-default-display-buffer))

;;;; Vhdl

;; (with-eval-after-load 'vhdl-mode
;;   (setq vhdl-file-header
;;	"-------------------------------------------------------------------------------
;; -- Title      : <title string>
;; -- Project    : <project>
;; -------------------------------------------------------------------------------
;; -- File       : <filename>
;; -- Author     : <author>
;; -- Company    : <company>
;; -- Created    : <date>
;; -- Last update: <date>
;; -- Platform   : <platform>
;; -- Standard   : <standard>
;; <projectdesc>-------------------------------------------------------------------------------
;; -- Description: <cursor>
;; <copyright>-------------------------------------------------------------------------------
;; -- Revisions  :
;; -- Date        Version  Author  Description
;; -- <date>  1.0      <login>	Created
;; -------------------------------------------------------------------------------
;; "
;;	))

(require 'lentic)
